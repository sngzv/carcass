package org.example.grpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;

public class MyServer extends GreetingServiceGrpc.GreetingServiceImplBase {

    @Override
    public void greeting(GreetingServiceOuterClass.HelloRequest request, StreamObserver<GreetingServiceOuterClass.HelloResponse> responseObserver) {

        System.out.println(request);

        GreetingServiceOuterClass.HelloResponse response = GreetingServiceOuterClass.
                HelloResponse.newBuilder().setGreeting("You send " + request.getName()).build();

        responseObserver.onNext(response);

        responseObserver.onCompleted();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        Server myServer = ServerBuilder
                .forPort(8082)
                .addService(new MyServer())
                .build();

        myServer.start();

        System.out.println("Server started");

        myServer.awaitTermination();
    }
}
