package org.example.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.entity.InfoEntity;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface InfoMapper {

    @Select("select * from info where id = #{id}")
    InfoEntity getById(Integer id);
}
