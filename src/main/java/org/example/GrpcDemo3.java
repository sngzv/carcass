package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpcDemo3 {
    public static void main(String[] args) {
        SpringApplication.run(GrpcDemo3.class, args);
    }
}
