package org.example.service;

import lombok.AllArgsConstructor;
import org.example.dao.InfoMapper;
import org.example.entity.InfoEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InfoService {

    private final InfoMapper mapper;

    public InfoEntity getInfoById(Integer id) {
        return mapper.getById(id);
    }
}
