package org.example.entity;

import lombok.Data;

@Data
public class InfoEntity {
    private Integer id;
    private String name;

    @Override
    public String toString() {
        return "InfoEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
